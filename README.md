# Erste Schritte mit Create React App

Dieses Projekt wurde mit [Create React App](https://github.com/facebook/create-react-app) gestartet.

## Verfügbare Skripte

Im Projektverzeichnis können Sie folgendes ausführen:

### `npm start`

Führt die Anwendung im Entwicklungsmodus aus.\
Öffne [http://localhost:3000](http://localhost:3000) um es im Browser anzuzeigen.

Die Seite wird neu geladen, wenn Sie Bearbeitungen vornehmen.\
Außerdem werden alle Lint-Fehler in der Konsole angezeigt.

### `npm test`

Startet den Test Runner im interaktiven Überwachungsmodus.\
Siehe den Abschnitt über [running tests](https://facebook.github.io/create-react-app/docs/running-tests) für weitere Informationen.

##

Mehr erfahren

Weitere Informationen finden Sie in der [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

Um React zu lernen, lesen Sie die [React-Dokumentation](https://reactjs.org/).

## Wichtig für das Login!!!

Um sich bei meiner Applikation einzuloggen benötigt man diese Login Daten: Email: irgendeine@email.adresse Passwort: m294

## Dokumentation der Entwicklungsumgebung

Auf VisualStudioCode habe ich das Projekt fertiggestellt, für mich ist das die beste Entwicklungsumgebung.

## Git Log

PS C:\Users\refsa\Documents\ModulKurs 294\uek-modul-294-portfolio> git log

commit 9943b5b9e624b983bbc0c7e35b6cc69b06c1a1d7 (HEAD -> master, origin/master)
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Wed Aug 31 13:53:47 2022 +0200

    Add function to manipulate Edit and Add and save new and existing Add

commit 3d5de0d185c4f0ecb36166c041cd7d8eb29f7103
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Wed Aug 31 13:13:33 2022 +0200

    Add doubleclick function

commit 83c7f8d2e721c2877d218eeef02e0d732e304f4c
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Wed Aug 31 13:11:24 2022 +0200

    Implement manipulated editable update/put

:...skipping...
commit 9943b5b9e624b983bbc0c7e35b6cc69b06c1a1d7 (HEAD -> master, origin/master)
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Wed Aug 31 13:53:47 2022 +0200

    Add function to manipulate Edit and Add and save new and existing Add

commit 3d5de0d185c4f0ecb36166c041cd7d8eb29f7103
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Wed Aug 31 13:13:33 2022 +0200

    Add doubleclick function

commit 83c7f8d2e721c2877d218eeef02e0d732e304f4c
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Wed Aug 31 13:11:24 2022 +0200

    Implement manipulated editable update/put

commit 1ea24b4f9b71592a275c8e88e8482b764c636f12
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Wed Aug 31 13:08:00 2022 +0200

    Add ESlint in script

commit 0396afe1178b7354a2085751d744ba18ec490a4d
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Wed Aug 31 13:05:07 2022 +0200

    Add login

commit b2ce421c1c27f74bd7c0d797497525fa46774515
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Tue Aug 30 15:58:27 2022 +0200

:...skipping...
commit 9943b5b9e624b983bbc0c7e35b6cc69b06c1a1d7 (HEAD -> master, origin/master)
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Wed Aug 31 13:53:47 2022 +0200

    Add function to manipulate Edit and Add and save new and existing Add

commit 3d5de0d185c4f0ecb36166c041cd7d8eb29f7103
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Wed Aug 31 13:13:33 2022 +0200

    Add doubleclick function

commit 83c7f8d2e721c2877d218eeef02e0d732e304f4c
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Wed Aug 31 13:11:24 2022 +0200

    Implement manipulated editable update/put

commit 1ea24b4f9b71592a275c8e88e8482b764c636f12
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Wed Aug 31 13:08:00 2022 +0200

    Add ESlint in script

commit 0396afe1178b7354a2085751d744ba18ec490a4d
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Wed Aug 31 13:05:07 2022 +0200

    Add login

commit b2ce421c1c27f74bd7c0d797497525fa46774515
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Tue Aug 30 15:58:27 2022 +0200

    Add delete function

commit 01c489f01bc1d760387504845c0c035aacf04f38
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Tue Aug 30 14:09:40 2022 +0200

    Implement DB

commit cf71d8f9b0e571665b8be88b3f6a8db7e0b27421
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Tue Aug 30 10:54:51 2022 +0200

    Add delete button/function

commit 9d7d446a8b88a63098c95ebbdc9982ef96495a0c
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Tue Aug 30 09:10:59 2022 +0200

    Changed generated files

commit c61090336c826297f54c8cce77bb4530b51ab6c1
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Tue Aug 30 08:52:32 2022 +0200

    Add generated files from React

:...skipping...
commit 9943b5b9e624b983bbc0c7e35b6cc69b06c1a1d7 (HEAD -> master, origin/master)
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Wed Aug 31 13:53:47 2022 +0200

    Add function to manipulate Edit and Add and save new and existing Add

commit 3d5de0d185c4f0ecb36166c041cd7d8eb29f7103
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Wed Aug 31 13:13:33 2022 +0200

    Add doubleclick function

commit 83c7f8d2e721c2877d218eeef02e0d732e304f4c
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Wed Aug 31 13:11:24 2022 +0200

    Implement manipulated editable update/put

commit 1ea24b4f9b71592a275c8e88e8482b764c636f12
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Wed Aug 31 13:08:00 2022 +0200

    Add ESlint in script

commit 0396afe1178b7354a2085751d744ba18ec490a4d
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Wed Aug 31 13:05:07 2022 +0200

    Add login

commit b2ce421c1c27f74bd7c0d797497525fa46774515
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Tue Aug 30 15:58:27 2022 +0200

    Add delete function

commit 01c489f01bc1d760387504845c0c035aacf04f38
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Tue Aug 30 14:09:40 2022 +0200

    Implement DB

commit cf71d8f9b0e571665b8be88b3f6a8db7e0b27421
commit 9943b5b9e624b983bbc0c7e35b6cc69b06c1a1d7 (HEAD -> master, origin/master)
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Wed Aug 31 13:53:47 2022 +0200

    Add function to manipulate Edit and Add and save new and existing Add

commit 3d5de0d185c4f0ecb36166c041cd7d8eb29f7103
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Wed Aug 31 13:13:33 2022 +0200

    Add doubleclick function

commit 83c7f8d2e721c2877d218eeef02e0d732e304f4c
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Wed Aug 31 13:11:24 2022 +0200

    Implement manipulated editable update/put

commit 1ea24b4f9b71592a275c8e88e8482b764c636f12
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Wed Aug 31 13:08:00 2022 +0200

    Add ESlint in script

commit 0396afe1178b7354a2085751d744ba18ec490a4d
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Wed Aug 31 13:05:07 2022 +0200

    Add login

commit b2ce421c1c27f74bd7c0d797497525fa46774515
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Tue Aug 30 15:58:27 2022 +0200

    Add delete function

commit 01c489f01bc1d760387504845c0c035aacf04f38
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Tue Aug 30 14:09:40 2022 +0200

    Implement DB

commit cf71d8f9b0e571665b8be88b3f6a8db7e0b27421
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Tue Aug 30 10:54:51 2022 +0200

    Add delete button/function

commit 9d7d446a8b88a63098c95ebbdc9982ef96495a0c
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Tue Aug 30 09:10:59 2022 +0200

    Changed generated files

commit c61090336c826297f54c8cce77bb4530b51ab6c1
Author: Refsat Bajra <refsat.bajra@gmail.com>
Date: Tue Aug 30 08:52:32 2022 +0200

    Add generated files from React

:

## Link zu Repo (Public)

https://gitlab.com/Refsat.Bajra/uek-modul-294-portfolio
