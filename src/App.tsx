import "./App.css";

import axios from "axios";
import { useEffect, useState } from "react";

import ManipulateTask from "./components/ManipulateTask";
import Login from "./components/login";
import TaskList from "./components/TaskList";
import ITask from "./models/Task";

const initialState: { tasks: ITask[]; editable?: ITask } = {
  tasks: [],
  editable: undefined,
};

const baseURL = "http://127.0.0.1:3000";

function App() {
  const title = document.getElementsByClassName("table_title")[0];
  const authConfig = {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("access_token")}`,
    },
  };
  const [state, setState] = useState(initialState);

  useEffect(() => {
    axios.get(`${baseURL}/auth/jwt/tasks`, authConfig).then((response) => {
      setState({ ...state, tasks: response.data });
      console.log(response.data);
      title.innerHTML = "Welcome you have successfully entered the To Do App";
    }); // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (!state) return null;

  function manipulateTask(task: ITask, editable: boolean): void {
    editable ? editTask(task) : saveTask(task);
  }

  function editTask(task: ITask) {
    setState({
      ...state,
      tasks: state.tasks.map((t) => (t.id === task.id ? task : t)),
    });
    axios.put(`${baseURL}/auth/jwt/tasks`, task, authConfig);
  }

  function saveTask(task: ITask) {
    const highestId = state.tasks.reduce(
      (acc, curr) => (acc < (curr.id || 1) ? curr.id! : acc),
      1
    );
    task.id = highestId + 1;
    setState({ ...state, tasks: [...state.tasks, task] });

    axios.post(`${baseURL}/auth/jwt/tasks`, task, authConfig);
  }

  function deleteTask(task: ITask) {
    let taskWithoutDeleted = state.tasks.filter(
      (currentTask) => task.id !== currentTask.id
    );

    axios.delete(`${baseURL}/auth/jwt/task/${task.id}`, authConfig).then(() => {
      setState({ ...state, tasks: taskWithoutDeleted });
      alert("Post deleted!");
    });
  }

  function resetEditable() {
    setState({ ...state, editable: undefined });
  }

  //if (!tasks) return "<div>No post!</div>";

  return (
    <div className="App">
      <TaskList
        tasks={state.tasks}
        deleteTask={deleteTask}
        selectTask={(editable) => setState({ ...state, editable })}
      ></TaskList>
      <ManipulateTask
        onManipulated={manipulateTask}
        editable={state.editable}
      ></ManipulateTask>
      {state.editable ? (
        <button onClick={resetEditable}>Create new task</button>
      ) : (
        ""
      )}
      <Login></Login>
    </div>
  );
}
export default App;

//Links:
//https://app.pluralsight.com/library/courses/react-js-getting-started/table-of-contents
