import ITask from "../models/Task";
export interface IProps {
  tasks: ITask[];
  selectTask: (task: ITask) => void;
  deleteTask: (task: ITask) => void;
}
function TaskList(props: IProps) {
  function remove(task: ITask) {
    props.deleteTask(task);
  }
  return (
    <div className="list">
      <table>
        <thead>
          <tr>
            <td>ID</td>
            <td>Titel</td>
            <td>Completed</td>
            <td>Actions</td>
          </tr>
        </thead>
        <tbody>
          {props.tasks.map((task) => {
            return (
              <tr key={task.id} onDoubleClick={() => props.selectTask(task)}>
                <td>{task.id}</td>
                <td>{task.title}</td>
                <td>{task.completed}</td>
                <td>
                  <button onClick={() => remove(task)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default TaskList;
