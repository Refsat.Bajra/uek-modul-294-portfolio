import { useEffect, useState } from "react";

import ITask from "../models/Task";

export interface IProps {
  onManipulated: (m: ITask, editable: boolean) => void;
  editable?: ITask;
}

const initTask: ITask = { title: "", id: 0, completed: false };

function ManipulateTask(props: IProps) {
  const [formValue, setFormValue] = useState<ITask>(initTask);

  useEffect(() => {
    setFormValue(props.editable || initTask);
  }, [props.editable]);

  const onInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setFormValue({ ...formValue, [name]: value });
  };

  function onFormSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    props.onManipulated(formValue, !!props.editable);
  }

  return (
    <div className="addTask">
      <h1 className="title">To Do</h1>
      <h2>{props.editable ? "Edit" : "Add"}</h2>
      <form className="formAdd" onSubmit={onFormSubmit}>
        <label>Task Description</label>
        <input
          type="text"
          placeholder="please input name"
          name="title"
          value={formValue.title}
          onChange={onInputChange}
        />
        <button>Save</button>
      </form>
    </div>
  );
}

export default ManipulateTask;

//Links:https://app.pluralsight.com/library/courses/react-apps-typescript-building/table-of-contents
