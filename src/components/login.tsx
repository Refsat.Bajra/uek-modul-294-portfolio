import axios from "axios";
import { useState } from "react";

const baseURL = "http://127.0.0.1:3000";

function Login() {
  const [credentials, changeCredentials] = useState({
    email: "",
    password: "",
  });

  const inputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    changeCredentials({ ...credentials, [name]: value });
  };

  function login(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    axios
      .post(`${baseURL}/auth/jwt/sign`, {
        email: credentials.email,
        password: credentials.password,
      })
      .then((response) => {
        localStorage.setItem("access_token", response.data.token);
        changeCredentials({ ...credentials });
      });
  }

  function logout(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    localStorage.removeItem("access_token");
    changeCredentials({ ...credentials });
  }

  function renderLogin() {
    if (localStorage.getItem("access_token")) {
      return (
        <form className="logoutForm" onSubmit={logout}>
          <button>Logout</button>
        </form>
      );
    } else {
      return (
        <form className="loginForm" onSubmit={login}>
          <input
            type="text"
            placeholder="email"
            name="email"
            value={credentials.email}
            onChange={inputChange}
          />
          <input
            type="password"
            placeholder="password"
            name="password"
            value={credentials.password}
            onChange={inputChange}
          />
          <button>Login</button>
        </form>
      );
    }
  }

  return (
    <div className="login">
      <h2>Login/Logout</h2>
      {renderLogin()}
    </div>
  );
}

export default Login;
